# Kovri Project's General Request for Funding

## Proposal

### *Who are you, or what is your handle (insert contact info here)?*

### *What is the nature of this request (i.e., what work would you like to complete or see completed by someone else)?*

### *How will this request be fulfilled and what is the desired outcome?*

### *Why is this outcome necessary for Kovri Project?*

### *If applicable, what are the milestones for this request?*

### *If applicable, how many hours a week will be needed to complete the request?*

### *When will this request be completed?*

### *Where will this request be fulfilled (e.g., internet, IRL location, etc.)?*

### *If outreach / marketing / sales: to whom do you aim to reach and why?*

## Funding

### *How should this proposal be funded (cryptocurrency, fiat, etc.)? Be specific.*

### *What is the expected rate for labor (e.g., N/per hour, N/per week, etc.)?*

### *In addition to proposed rate, how much funding will be needed for additional resources? If any, define those resources here.*
