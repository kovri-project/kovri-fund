# Bitcoin / Litcoin Multisig

## Setup
All of this should work in the RPC Console of the GUIs for both Bitcoin/Litecoin
QT clients without having to add any configuration (I think). If you want
`bitcoind` to listen for rpc calls you will have to set some og the following
options in the `bitcoin.conf` file or pass as arguments on the command line:

### example bitcoin.conf file
```
server=1
rest=1
# realnet bitcoin default = 8332
# realnet litecoin default = 9332
# testnet bitcoin default = 18332
# testnet litecoin default = 19332
rpcport=8332
rpcbind=127.0.0.1:8332
# only allow localhost just in case
rpcallowip=127.0.0.0/8
# the rpc client will reference this same config file
# so no need to remember this unless it's non-local
rpcuser=madeupuser
rpcpassword=longnonsensepasswordyoudontneedtoremember
```

You can now make rpc calls to the daemon using the command line.
You will need to make sure `--disablewallet` is not set (or is set to 0) if 
you want to be able to issue the following commands.

## I Really Did Do This
I really did all of the steps below with actual testnet Litecoins, was able to
create the multisig address from 3 individual addresses, send coins to it all
3 variants (legacy/segwit/bech32) and then send a transaction back out to a 
normal address. This was probably easier because I had all the keys, and already
loaded into the wallet, but the general principle is still shown (I think). If
you want to use [chain.so](https://chain.so/address/LTCTEST/2N6J5QPeNJHzGzM63VX93hZ6UGANQiPDW1R) to
follow along, all of the txids and and addresses that got sent should be on
there.

## Start with 3 normal addresses
Each of the custodians generates any kind of address in the safest way the can.
Ideally on a hardware wallet so that the private key is never exposed. We only
need the public address to create the multisig address and we only need to be
able to sign with some combination of 2 of our individual private keys to be
able to spend an input.

For reference, I am using this alias to make the rpc calls:
`alias litcoin-cli="sudo -sHu litecoin litecoin-cli -conf=/etc/litecoin-test.conf -testnet=1"`

I generated 3 testnet litecoin address (because that what I have laying around),
but just pretend we each made them individually and very carefully:

  * `litecoin-cli getnewaddress "luke's very safe address"`
    - mo61jY15yRHJcUgiZbEVeW7MJMbKiweB5V
  * `litecoin-cli getnewaddress "sean's very safe address"`
    - moFR1pR3zPMSsk5ZEsg42wLG2Z3SckCZ8H
  * `litecoin-cli getnewaddress "anonimal's very safe address"`
    - mzkaaT6u8MXfk6aNnmZVuTk16womqGaaYY

Once this is done, these public keys can be comined into a single multisig
address with a 2 signatures from any of the 3 corresponding public addresses
required to spend.

The command for creating this has the following usage:
`addmultisigaddress `req_int` '["addrORpubkeys"]' 'optional-name' '(legacy|p2sh-segwit|bech32)'`

This creates the pubkey of the multisig address. You can also do this manually
by using `litecoin-cli validateaddress mo61jY15yRHJcUgiZbEVeW7MJMbKiweB5V`
which will output the address's details including pubkey. I think this is the
prefered way to do it, but it comes out the same if we use pubkeys, which are
harder to mistype and actually have checksums so... idk there.

## Address Format
So a 2/3 multisig address in each of the three formats would be generated and
look like this (on the LTC testnet):

### Legacy = [QQjtn3sHhDhDdrTVmnPJwPjRPbKrMn89nP](https://chain.so/address/LTCTEST/QQjtn3sHhDhDdrTVmnPJwPjRPbKrMn89nP)
`litecoin-cli addmultisigaddress 2 '["mo61jY15yRHJcUgiZbEVeW7MJMbKiweB5V","moFR1pR3zPMSsk5ZEsg42wLG2Z3SckCZ8H","mzkaaT6u8MXfk6aNnmZVuTk16womqGaaYY"]' kovri-fund "legacy"`
 
### P2SH-SegWit = [Qeeh7hgjLss7qZV3ZkZCcJD83RoNiHtniC](https://chain.so/address/LTCTEST/Qeeh7hgjLss7qZV3ZkZCcJD83RoNiHtniC)
`litecoin-cli addmultisigaddress 2 '["mo61jY15yRHJcUgiZbEVeW7MJMbKiweB5V","moFR1pR3zPMSsk5ZEsg42wLG2Z3SckCZ8H","mzkaaT6u8MXfk6aNnmZVuTk16womqGaaYY"]' kovri-fund "p2sh-segwit"`

### bech32 = [tltc1qgcdgza62w4s7md90ch7vgt706waflgtvyqnvnxalfxs5fp4zg2nq362gyj](https://chain.so/address/LTCTEST/tltc1qgcdgza62w4s7md90ch7vgt706waflgtvyqnvnxalfxs5fp4zg2nq362gyj)
`litecoin-cli addmultisigaddress 2 '["mo61jY15yRHJcUgiZbEVeW7MJMbKiweB5V","moFR1pR3zPMSsk5ZEsg42wLG2Z3SckCZ8H","mzkaaT6u8MXfk6aNnmZVuTk16womqGaaYY"]' kovri-fund "bech32"`

## Confirming Transactions
I sent different amount to the 3 address formats respectively to ensure none
cause problems with multisig, they didn't.

  * `litecoin-cli sendtoaddress "QQjtn3sHhDhDdrTVmnPJwPjRPbKrMn89nP" 111`
    - [2f0b5714103492bbba5f8dab2f50aad1551af1a2fdc8503aa6146ee511dbbf39](https://chain.so/tx/LTCTEST/2f0b5714103492bbba5f8dab2f50aad1551af1a2fdc8503aa6146ee511dbbf39)
  * `litecoin-cli sendtoaddress "Qeeh7hgjLss7qZV3ZkZCcJD83RoNiHtniC" 222`
    - [b6a59aaf14aad53cdb72a596d17675b3dd638c734f8cc426c7a720e1802d2f3a](https://chain.so/tx/LTCTEST/b6a59aaf14aad53cdb72a596d17675b3dd638c734f8cc426c7a720e1802d2f3a)
  * `litecoin-cli sendtoaddress "tltc1qgcdgza62w4s7md90ch7vgt706waflgtvyqnvnxalfxs5fp4zg2nq362gyj" 333`
    - [01f0c85c0e2325b8887243b8d56fbe28ce68f34010e658a3030e5c20530955d8](https://chain.so/tx/LTCTEST/01f0c85c0e2325b8887243b8d56fbe28ce68f34010e658a3030e5c20530955d8)
    
We can confirm by looking at the txids individually with:
  * `litecoin-cli gettransaction 2f0b5714103492bbba5f8dab2f50aad1551af1a2fdc8503aa6146ee511dbbf39`
  * `litecoin-cli gettransaction b6a59aaf14aad53cdb72a596d17675b3dd638c734f8cc426c7a720e1802d2f3a`
  * `litecoin-cli gettransaction 01f0c85c0e2325b8887243b8d56fbe28ce68f34010e658a3030e5c20530955d8`
Which prints a bunch of verbose information, including non-zero block confirmations.

And the balance for the account is what we would expect:
  * `litecoin-cli getbalance kovri-fund`
    - 666.00000000

## Redeem Script
The redeem script is the same regardless of which formatting is used for the 
public address. In this case it is: 

```
5221029bfc8e83730dc0bca2a321db535a044b866db1757f7aca0df0f9814869cceaf52102f8162f
185020b4cebe752b904b7fadf22604b02accf17afbecbc1abbf30093f421025a21de811363d282af
d2971a3e58731bc9e495375e4acbb2cd571e03ab8ecf4153ae
```

## Spending Multisig 
This is where it gets a bit (very) confusing, but once we get the process down
correctly, we can basically just sign keys and script it. In this example, I am
paying 332.9 LTCt to [2N6J5QPeNJHzGzM63VX93hZ6UGANQiPDW1R](https://chain.so/address/LTCTEST/2N6J5QPeNJHzGzM63VX93hZ6UGANQiPDW1R) from the last 333 coin
input transaction:

  1. Find an unspend input of the multisig address with `listunspent`. 
     - `01f0c85c0e2325b8887243b8d56fbe28ce68f34010e658a3030e5c20530955d8`
  
  2. Create a raw transaction by specifying that unspent input, and output address, and an amount (leaving a .1 LTC mining fee behind):
     - `litecoin-cli createrawtransaction '[{"txid" : "01f0c85c0e2325b8887243b8d56fbe28ce68f34010e658a3030e5c20530955d8","vout":0}]' '{"2N6J5QPeNJHzGzM63VX93hZ6UGANQiPDW1R":332.9}'`

```
0200000001d8550953205c0e03a358e61040f368ce28be6fd5b8437288b825230e5cc8f00100000
00000ffffffff0100ccdfba0700000017a9148f23b79b55883cc1c2d435e82c8993bc15f8b20a87
00000000
```

  3. This is then signed (which can be done in via the app interface in many HW wallets) or with the RPC, until it has enough signatures to be valid.
  - `litecoin-cli signrawtransaction 0200000001d8550953205c0e03a358e61040
     f368ce28be6fd5b8437288b825230e5cc8f0010000000000ffffffff0100ccdfba070
     0000017a9148f23b79b55883cc1c2d435e82c8993bc15f8b20a8700000000`

```
0200000001d8550953205c0e03a358e61040f368ce28be6fd5b8437288b825230e5cc8f00100000
0006a47304402206f0eee95a1f3ab9f799164e6678cc3fcaf785859bbd40735444edb86b0283c95
02201f37fb09e82d0eb9bcb76cd9db429d28fa40f67598657de0af7adaa8bc1a6a350121038861a
a31804abf42e6dbf87f83770d2c8441bb59424f4fd851cff72e06887d44ffffffff0180163dc007
00000017a9148f23b79b55883cc1c2d435e82c8993bc15f8b20a8700000000
```

  4. Then it's sent.
  `litecoin-cli sendrawtransaction 0200000001d8550953205c0e03a358e61040f368ce28
  be6fd5b8437288b825230e5cc8f001000000006a47304402206f0eee95a1f3ab9f799164e6678cc3f
  caf785859bbd40735444edb86b0283c9502201f37fb09e82d0eb9bcb76cd9db429d28fa40f6759865
  7de0af7adaa8bc1a6a350121038861aa31804abf42e6dbf87f83770d2c8441bb59424f4fd851cff72
  e06887d44ffffffff0180163dc00700000017a9148f23b79b55883cc1c2d435e82c8993bc15f8b20a
  8700000000`

  5. TADA [cd11cc5649f578ded375ae8a5bf5fd38bc53432146db2513ed654be9db359b89](https://chain.so/tx/LTCTEST/cd11cc5649f578ded375ae8a5bf5fd38bc53432146db2513ed654be9db359b89)


## ** Gavin Andresen does a [more thorough job](https://gist.github.com/gavinandresen/3966071) of explaining this gist.*
